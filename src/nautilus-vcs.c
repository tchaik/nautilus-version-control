/*
 * nautilus-vcs: VCS abstration interface definition.
 *
 * Copyright (C) 2017, Martin Blanchard <tchaik@gmx.com>
 *
 * Nautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * Nautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Martin Blanchard <tchaik@gmx.com>
 *
 */

#include <glib.h>

#include "nautilus-vcs.h"

#define G_LOG_DOMAIN "nautilus-vcs"

G_DEFINE_INTERFACE (NautilusVcs, nautilus_vcs,
                    G_TYPE_OBJECT)

enum
{
    PROP_LOCATION = 1,
    PROP_WORKING_DIRECTORY,
    NUM_PROPERTIES
};

static GParamSpec *properties[NUM_PROPERTIES] = { NULL };
static GArray *types = NULL;

gchar *
nautilus_vcs_get_name (NautilusVcs *self)
{
    g_return_val_if_fail (NAUTILUS_IS_VCS (self), NULL);

    if (NAUTILUS_VCS_GET_IFACE (self)->get_name)
        return NAUTILUS_VCS_GET_IFACE (self)->get_name (self);

    return NULL;
}

GFile *
nautilus_vcs_get_location (NautilusVcs *self)
{
    g_return_val_if_fail (NAUTILUS_IS_VCS (self), NULL);

    if (NAUTILUS_VCS_GET_IFACE (self)->get_location)
        return NAUTILUS_VCS_GET_IFACE (self)->get_location (self);

    return NULL;
}

GPtrArray *
nautilus_vcs_get_branch_list (NautilusVcs *self)
{
    g_return_val_if_fail (NAUTILUS_IS_VCS (self), NULL);

    if (NAUTILUS_VCS_GET_IFACE (self)->get_branch_list)
        return NAUTILUS_VCS_GET_IFACE (self)->get_branch_list (self);

    return NULL;
}

gchar *
nautilus_vcs_get_active_branch (NautilusVcs *self)
{
    g_return_val_if_fail (NAUTILUS_IS_VCS (self), NULL);

    if (NAUTILUS_VCS_GET_IFACE (self)->get_active_branch)
        return NAUTILUS_VCS_GET_IFACE (self)->get_active_branch (self);

    return NULL;
}

gchar *
nautilus_vcs_get_head_id (NautilusVcs *self)
{
    g_return_val_if_fail (NAUTILUS_IS_VCS (self), NULL);

    if (NAUTILUS_VCS_GET_IFACE (self)->get_head_id)
        return NAUTILUS_VCS_GET_IFACE (self)->get_head_id (self);

    return NULL;
}

gchar *
nautilus_vcs_get_head_message (NautilusVcs *self)
{
    g_return_val_if_fail (NAUTILUS_IS_VCS (self), NULL);

    if (NAUTILUS_VCS_GET_IFACE (self)->get_head_message)
        return NAUTILUS_VCS_GET_IFACE (self)->get_head_message (self);

    return NULL;
}

gchar *
nautilus_vcs_get_head_oneline (NautilusVcs *self)
{
    g_return_val_if_fail (NAUTILUS_IS_VCS (self), NULL);

    if (NAUTILUS_VCS_GET_IFACE (self)->get_head_oneline)
        return NAUTILUS_VCS_GET_IFACE (self)->get_head_oneline (self);

    return NULL;
}

gchar *
nautilus_vcs_get_application_name (NautilusVcs *self)
{
    g_return_val_if_fail (NAUTILUS_IS_VCS (self), NULL);

    if (NAUTILUS_VCS_GET_IFACE (self)->get_application_name)
        return NAUTILUS_VCS_GET_IFACE (self)->get_application_name (self);

    return NULL;
}

static void
nautilus_vcs_default_init (NautilusVcsInterface *iface)
{
    iface->get_name = nautilus_vcs_get_name;
    iface->get_location = nautilus_vcs_get_location;
    iface->get_branch_list = nautilus_vcs_get_branch_list;
    iface->get_active_branch = nautilus_vcs_get_active_branch;
    iface->get_head_id = nautilus_vcs_get_head_id;
    iface->get_head_message = nautilus_vcs_get_head_message;
    iface->get_head_oneline = nautilus_vcs_get_head_oneline;
    iface->get_application_name = nautilus_vcs_get_application_name;

    properties[PROP_LOCATION] =
        g_param_spec_object ("location",
                             "Location",
                             "The original location inside the VCS repository",
                             G_TYPE_FILE,
                             (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT_ONLY));
    g_object_interface_install_property (iface, properties[PROP_LOCATION]);
}

void
nautilus_vcs_register_type (GType type_id)
{
    if (types == NULL)
        types = g_array_new (FALSE, TRUE, sizeof (GType));

    g_assert (types);

    g_array_append_val (types, type_id);
}

NautilusVcs *
nautilus_vcs_discover (GFile   *location,
                       GError **error)
{
    NautilusVcs *vcs = NULL;
    gint i;

    g_return_val_if_fail (G_IS_FILE (location), NULL);

    if (!types || types->len == 0)
    {
        g_set_error (error,
                     G_IO_ERROR,
                     G_IO_ERROR_NOT_INITIALIZED,
                     "Extension module must be initialized first.");
        return NULL;
    }

    for (i = 0; i < types->len; i++)
    {
        GType type_id;

        type_id = g_array_index (types, GType, i);
        vcs = g_initable_new (type_id, NULL, error, "location", location, NULL);

        if (vcs != NULL)
            break;
    }

    return vcs;
}

/* vim: set tabstop=4 shiftwidth=0 expandtab : */
