/*
 * nautilus-vcs-git: Git VCS interface implementation.
 *
 * Copyright (C) 2017, Martin Blanchard <tchaik@gmx.com>
 *
 * Nautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * Nautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Martin Blanchard <tchaik@gmx.com>
 *
 */

#ifndef __NAUTILUS_VCS_GIT_H__
#define __NAUTILUS_VCS_GIT_H__

#include <glib-object.h>

G_BEGIN_DECLS

#define NAUTILUS_TYPE_VCS_GIT (nautilus_vcs_git_get_type())

G_DECLARE_FINAL_TYPE (NautilusVcsGit, nautilus_vcs_git,
                      NAUTILUS, VCS_GIT,
                      GObject)

G_END_DECLS

#endif /* __NAUTILUS_VCS_GIT_H__ */

/* vim: set tabstop=4 shiftwidth=0 expandtab : */
