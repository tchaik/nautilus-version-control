/*
 * nautilus-version-control: extension module registration.
 *
 * Copyright (C) 2017, Martin Blanchard <tchaik@gmx.com>
 *
 * Nautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * Nautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Martin Blanchard <tchaik@gmx.com>
 *
 */

#include <config.h>

#include <stdlib.h>
#include <string.h>

#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>
#include <gtk/gtk.h>

#include <libnautilus-extension/nautilus-location-widget-provider.h>

#include "nautilus-vcs.h"
#include "nautilus-vcs-git.h"
#include "nautilus-vcs-location-widget.h"

#define NAUTILUS_TYPE_VERSION_CONTROL (nautilus_version_control_get_type())

G_DECLARE_FINAL_TYPE (NautilusVersionControl, nautilus_version_control, NAUTILUS, VERSION_CONTROL, GObject)

struct _NautilusVersionControl
{
    GObject parent_instance;
};

static void nautilus_version_control_location_widget_provider_init (NautilusLocationWidgetProviderIface *iface);

G_DEFINE_DYNAMIC_TYPE_EXTENDED (NautilusVersionControl, nautilus_version_control, G_TYPE_OBJECT, 0,
                                G_IMPLEMENT_INTERFACE_DYNAMIC(NAUTILUS_TYPE_LOCATION_WIDGET_PROVIDER,
                                                              nautilus_version_control_location_widget_provider_init))

static void
nautilus_version_control_class_init (NautilusVersionControlClass *class)
{
    GObjectClass *object_class;

    object_class = G_OBJECT_CLASS (class);
}

static void
nautilus_version_control_class_finalize (NautilusVersionControlClass *class)
{
}

static void
nautilus_version_control_init (NautilusVersionControl *self)
{
#ifdef HAVE_GIT
    nautilus_vcs_register_type (NAUTILUS_TYPE_VCS_GIT);
#endif
}

GtkWidget *
nautilus_version_control_location_widget_provider_get_widget (NautilusLocationWidgetProvider *provider,
                                                              const char                     *uri,
                                                              GtkWidget                      *window)
{
    g_autoptr(GtkWidget) location_widget = NULL;
    g_autoptr(NautilusVcs) vcs = NULL;
    g_autoptr(GFile) file = NULL;
    GError *error = NULL;

    g_return_val_if_fail (NAUTILUS_IS_VERSION_CONTROL (provider), NULL);

    file = g_file_new_for_uri (uri);

    if ((vcs = nautilus_vcs_discover (file, &error)) == NULL)
    {
        if (g_error_matches (error, G_IO_ERROR, G_IO_ERROR_NOT_SUPPORTED))
            g_warning ("%s", error->message);
        else if (!g_error_matches (error, G_IO_ERROR, G_IO_ERROR_NOT_FOUND))
            g_message ("%s", error->message);
        g_clear_error (&error);
        return NULL;
    }

    if ((location_widget = nautilus_vcs_location_widget_new (vcs)) == NULL)
    {
        g_warning ("Location widget creation failed");
        return NULL;
    }

    return g_object_ref (location_widget);
}

static void
nautilus_version_control_location_widget_provider_init (NautilusLocationWidgetProviderIface *iface)
{
    iface->get_widget = nautilus_version_control_location_widget_provider_get_widget;
}

static GType type_list[1];

void
nautilus_module_initialize (GTypeModule *module)
{
    g_print ("Initializing nautilus-version-control extension.\n");

    nautilus_version_control_register_type (module);

    type_list[0] = NAUTILUS_TYPE_VERSION_CONTROL;
}

void
nautilus_module_shutdown (void)
{
    g_print ("Shutting down nautilus-version-control extension.\n");
}

void 
nautilus_module_list_types (const GType **types,
                            int          *num_types)
{
    *types = type_list;
    *num_types = G_N_ELEMENTS (type_list);
}

/* vim: set tabstop=4 shiftwidth=0 expandtab : */
