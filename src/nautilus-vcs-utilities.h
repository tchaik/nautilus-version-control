/*
 * nautilus-vcs-utilities: various helper functions.
 *
 * Copyright (C) 2017, Martin Blanchard <tchaik@gmx.com>
 *
 * Nautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * Nautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Martin Blanchard <tchaik@gmx.com>
 *
 */

#ifndef __NAUTILUS_VCS_UTILITIES_H__
#define __NAUTILUS_VCS_UTILITIES_H__

#include <glib-object.h>
#include <gio/gio.h>

G_BEGIN_DECLS

typedef struct {
    gchar               *command_line;
    gchar               *application_name;
    GList               *file_list;
    GAppInfoCreateFlags  flags;
} NautilusVcsAppContext;

void      nautilus_vcs_start_application_async   (gpointer             source_object,
                                                  GCancellable        *cancellable,
                                                  GAsyncReadyCallback  callback,
                                                  gpointer             user_data);

gboolean  nautilus_vcs_start_application_finish  (gpointer       source_object,
                                                  GAsyncResult  *result,
                                                  GError       **error);

G_END_DECLS

#endif /* __NAUTILUS_VCS_UTILITIES_H__ */

/* vim: set tabstop=4 shiftwidth=0 expandtab : */
