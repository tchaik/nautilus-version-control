/*
 * nautilus-vcs-location-widget: main VCS widget.
 *
 * Copyright (C) 2017, Martin Blanchard <tchaik@gmx.com>
 *
 * Nautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * Nautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Martin Blanchard <tchaik@gmx.com>
 *
 */

#include <glib.h>

#include "nautilus-vcs-location-widget.h"
#include "nautilus-vcs-utilities.h"

#define G_LOG_DOMAIN "nautilus-vcs-location-widget"

typedef struct _NautilusVcsLocationWidgetPrivate
{
    NautilusVcs *vcs;

    GtkWidget   *location_info_bar;

    GtkWidget   *active_branch_label;
    GtkWidget   *head_label;

    GtkWidget   *more_button;
} NautilusVcsLocationWidgetPrivate;

struct _NautilusVcsLocationWidget
{
    GtkRevealer                       parent_instance;

    NautilusVcsLocationWidgetPrivate *priv;
};

enum
{
    PROP_VCS = 1,
    NUM_PROPERTIES,
};

static GParamSpec *properties[NUM_PROPERTIES] = { NULL };

G_DEFINE_TYPE_WITH_PRIVATE (NautilusVcsLocationWidget, nautilus_vcs_location_widget,
                            GTK_TYPE_REVEALER)

static NautilusVcs *
nautilus_vcs_location_widget_get_vcs (NautilusVcsLocationWidget *self)
{
    g_return_val_if_fail (NAUTILUS_IS_VCS_LOCATION_WIDGET (self), NULL);

    return self->priv->vcs;
}

static void
nautilus_vcs_location_widget_set_vcs (NautilusVcsLocationWidget *self,
                                      NautilusVcs               *vcs)
{
    g_return_if_fail (NAUTILUS_IS_VCS_LOCATION_WIDGET (self));

    g_set_object (&self->priv->vcs, vcs);
}

static void
nautilus_vcs_location_widget_finalize (GObject *object)
{
    NautilusVcsLocationWidget *self = NAUTILUS_VCS_LOCATION_WIDGET (object);

    g_clear_object (&self->priv->vcs);

    G_OBJECT_CLASS (nautilus_vcs_location_widget_parent_class)->finalize (object);
}

static void
nautilus_vcs_location_widget_get_property (GObject    *object,
                                           guint       prop_id,
                                           GValue     *value,
                                           GParamSpec *pspec)
{
    NautilusVcsLocationWidget *self = NAUTILUS_VCS_LOCATION_WIDGET (object);

    switch (prop_id)
    {
        case PROP_VCS:
            g_value_set_object (value, nautilus_vcs_location_widget_get_vcs (self));
            break;

        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
nautilus_vcs_location_widget_set_property (GObject      *object,
                                           guint         prop_id,
                                           const GValue *value,
                                           GParamSpec   *pspec)
{
    NautilusVcsLocationWidget *self = NAUTILUS_VCS_LOCATION_WIDGET (object);

    switch (prop_id)
    {
        case PROP_VCS:
            nautilus_vcs_location_widget_set_vcs (self, g_value_get_object (value));
            break;

        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
nautilus_vcs_location_widget_class_init (NautilusVcsLocationWidgetClass *class)
{
    GObjectClass *object_class;
    GtkWidgetClass *widget_class;

    object_class = G_OBJECT_CLASS (class);
    widget_class = GTK_WIDGET_CLASS (class);

    object_class->finalize = nautilus_vcs_location_widget_finalize;
    object_class->get_property = nautilus_vcs_location_widget_get_property;
    object_class->set_property = nautilus_vcs_location_widget_set_property;

    properties[PROP_VCS] =
        g_param_spec_object ("vcs",
                             "VCS",
                             "The VCS handler object",
                             NAUTILUS_TYPE_VCS,
                             (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT_ONLY));

    g_object_class_install_properties (object_class, NUM_PROPERTIES, properties);

    gtk_widget_class_set_template_from_resource (widget_class,
        "/org/gnome/nautilus-version-control/nautilus-vcs-location-widget.ui");

    gtk_widget_class_bind_template_child_private (widget_class,
                                                  NautilusVcsLocationWidget,
                                                  location_info_bar);

    gtk_widget_class_bind_template_child_private (widget_class,
                                                  NautilusVcsLocationWidget,
                                                  active_branch_label);

    gtk_widget_class_bind_template_child_private (widget_class,
                                                  NautilusVcsLocationWidget,
                                                  head_label);

    gtk_widget_class_bind_template_child_private (widget_class,
                                                  NautilusVcsLocationWidget,
                                                  more_button);
}

static void
nautilus_vcs_location_widget_init (NautilusVcsLocationWidget *self)
{
    self->priv = nautilus_vcs_location_widget_get_instance_private (self);

    gtk_widget_init_template (GTK_WIDGET (self));
}

static void
nautilus_vcs_location_widget_start_application_cb (GObject      *source_object,
                                                   GAsyncResult *result,
                                                   gpointer      user_data)
{
    NautilusVcsLocationWidget *self = (NautilusVcsLocationWidget *)source_object;
    GError *error = NULL;
    gboolean status;

    g_return_if_fail (NAUTILUS_IS_VCS_LOCATION_WIDGET (self));

    status = nautilus_vcs_start_application_finish (self, result, &error);

    if (!status || error != NULL)
    {

    }
}

static void
nautilus_vcs_location_widget_on_more (GtkButton *widget,
                                      gpointer   user_data)
{
    NautilusVcsLocationWidget *self = (NautilusVcsLocationWidget *)user_data;
    NautilusVcsAppContext *context = g_new0 (NautilusVcsAppContext, 1);
    g_autofree gchar *application_name = NULL;
    GList *file_list = NULL;
    GFile *location;

    g_return_if_fail (NAUTILUS_IS_VCS_LOCATION_WIDGET (self));
    g_return_if_fail (context != NULL);

    application_name = nautilus_vcs_get_application_name (self->priv->vcs);
    location = nautilus_vcs_get_location (self->priv->vcs);
    file_list = g_list_append (file_list, location);

    context->application_name = g_strdup (application_name);
    context->file_list = file_list;

    nautilus_vcs_start_application_async (self,
                                          NULL,
                                          nautilus_vcs_location_widget_start_application_cb,
                                          context);
}

GtkWidget *
nautilus_vcs_location_widget_new (NautilusVcs *vcs)
{
    NautilusVcsLocationWidget *self;

    g_return_val_if_fail (NAUTILUS_IS_VCS (vcs), NULL);

    self = g_object_new (NAUTILUS_TYPE_VCS_LOCATION_WIDGET, "vcs", vcs, NULL);

    gtk_label_set_text (GTK_LABEL (self->priv->active_branch_label),
                        nautilus_vcs_get_active_branch (vcs));

    gtk_label_set_markup (GTK_LABEL (self->priv->head_label),
                          nautilus_vcs_get_head_oneline (vcs));

    g_signal_connect (G_OBJECT (self->priv->more_button),
                      "clicked",
                      (GCallback) nautilus_vcs_location_widget_on_more,
                      self);

    gtk_revealer_set_transition_type (GTK_REVEALER (self),
                                      GTK_REVEALER_TRANSITION_TYPE_SLIDE_DOWN),
    gtk_revealer_set_reveal_child (GTK_REVEALER (self), TRUE);

    gtk_container_add (GTK_CONTAINER (self), self->priv->location_info_bar);

    gtk_widget_show_all (GTK_WIDGET (self));

    return GTK_WIDGET (self);
}

/* vim: set tabstop=4 shiftwidth=0 expandtab : */
