/*
 * nautilus-vcs-utilities: various helper functions.
 *
 * Copyright (C) 2017, Martin Blanchard <tchaik@gmx.com>
 *
 * Nautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * Nautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Martin Blanchard <tchaik@gmx.com>
 *
 */

#include "nautilus-vcs-utilities.h"

#define G_LOG_DOMAIN "nautilus-vcs-utilities"

void
nautilus_vcs_start_application_worker (GTask        *task,
                                       gpointer      source_object,
                                       gpointer      task_data,
                                       GCancellable *cancellable)
{
    NautilusVcsAppContext *context = (NautilusVcsAppContext *) task_data;
    g_autoptr(GAppInfo) app_info = NULL;
    GError *error = NULL;
    gboolean status;

    g_assert (context != NULL);
    g_assert (G_IS_TASK (task));
    g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

    if (context->command_line == NULL)
        context->command_line = g_strdup (context->application_name);

    app_info = g_app_info_create_from_commandline (context->command_line,
                                                   context->application_name,
                                                   context->flags,
                                                   &error);

    status = g_app_info_launch (app_info, context->file_list, NULL, &error);

    if (error != NULL)
    {
        g_task_return_error (task, error);
        return;
    }

    g_task_return_boolean (task, status);

    g_free (context);
}

void
nautilus_vcs_start_application_async (gpointer             source_object,
                                      GCancellable        *cancellable,
                                      GAsyncReadyCallback  callback,
                                      gpointer             user_data)
{
    g_autoptr(GTask) task = NULL;

    task = g_task_new (source_object, cancellable, callback, source_object);
    g_task_set_task_data (task, user_data, g_free);

    g_task_run_in_thread (g_object_ref (task),
                          nautilus_vcs_start_application_worker);
}

gboolean
nautilus_vcs_start_application_finish (gpointer       source_object,
                                       GAsyncResult  *result,
                                       GError       **error)
{
    GTask *task = (GTask *)result;
    gboolean status;

    g_return_val_if_fail (G_IS_TASK (task), FALSE);
    g_return_val_if_fail (g_task_is_valid (task, source_object), FALSE);

    status = g_task_propagate_boolean (task, error);

    return status;
}

/* vim: set tabstop=4 shiftwidth=0 expandtab : */
