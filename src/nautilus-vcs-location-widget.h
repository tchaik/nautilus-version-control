/*
 * nautilus-vcs-location-widget: main VCS widget.
 *
 * Copyright (C) 2017, Martin Blanchard <tchaik@gmx.com>
 *
 * Nautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * Nautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Martin Blanchard <tchaik@gmx.com>
 *
 */

#ifndef __NAUTILUS_VCS_LOCATION_WIDGET_H__
#define __NAUTILUS_VCS_LOCATION_WIDGET_H__

#include <glib-object.h>
#include <gtk/gtk.h>

#include "nautilus-vcs.h"

G_BEGIN_DECLS

#define NAUTILUS_TYPE_VCS_LOCATION_WIDGET (nautilus_vcs_location_widget_get_type())

G_DECLARE_FINAL_TYPE (NautilusVcsLocationWidget, nautilus_vcs_location_widget,
                      NAUTILUS, VCS_LOCATION_WIDGET,
                      GtkRevealer)

GtkWidget *        nautilus_vcs_location_widget_new  (NautilusVcs *vcs);

G_END_DECLS

#endif /* __NAUTILUS_VCS_LOCATION_WIDGET_H__ */

/* vim: set tabstop=4 shiftwidth=0 expandtab : */
