/*
 * nautilus-vcs: VCS abstration interface definition.
 *
 * Copyright (C) 2017, Martin Blanchard <tchaik@gmx.com>
 *
 * Nautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * Nautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Martin Blanchard <tchaik@gmx.com>
 *
 */

#ifndef __NAUTILUS_VCS_H__
#define __NAUTILUS_VCS_H__

#include <glib-object.h>
#include <gio/gio.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define NAUTILUS_TYPE_VCS (nautilus_vcs_get_type())

G_DECLARE_INTERFACE (NautilusVcs, nautilus_vcs,
                     NAUTILUS, VCS,
                     GObject)

struct _NautilusVcsInterface
{
    GTypeInterface    parent_interface;

    gchar *         (*get_name)                  (NautilusVcs *self);

    GFile *         (*get_location)              (NautilusVcs *self);

    GPtrArray *     (*get_branch_list)           (NautilusVcs *self);
    gchar *         (*get_active_branch)         (NautilusVcs *self);

    gchar *         (*get_head_id)               (NautilusVcs *self);
    gchar *         (*get_head_message)          (NautilusVcs *self);
    gchar *         (*get_head_oneline)          (NautilusVcs *self);

    gchar *         (*get_application_name)      (NautilusVcs *self);
};

void               nautilus_vcs_register_type         (GType type_id);

NautilusVcs *      nautilus_vcs_discover              (GFile   *location,
                                                       GError **error);

gchar *            nautilus_vcs_get_name              (NautilusVcs *self);

GFile *            nautilus_vcs_get_location          (NautilusVcs *self);

GPtrArray *        nautilus_vcs_get_branch_list       (NautilusVcs *self);
gchar *            nautilus_vcs_get_active_branch     (NautilusVcs *self);

gchar *            nautilus_vcs_get_head_id           (NautilusVcs *self);
gchar *            nautilus_vcs_get_head_message      (NautilusVcs *self);
gchar *            nautilus_vcs_get_head_oneline      (NautilusVcs *self);

gchar *            nautilus_vcs_get_application_name  (NautilusVcs *self);

G_END_DECLS

#endif /* __NAUTILUS_VCS_H__ */

/* vim: set tabstop=4 shiftwidth=0 expandtab : */
