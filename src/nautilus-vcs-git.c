/*
 * nautilus-vcs-git: Git VCS interface implementation.
 *
 * Copyright (C) 2017, Martin Blanchard <tchaik@gmx.com>
 *
 * Nautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * Nautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Martin Blanchard <tchaik@gmx.com>
 *
 */

#include <glib.h>
#include <gio/gio.h>

#include <libgit2-glib/ggit.h>

#include "nautilus-vcs-git.h"
#include "nautilus-vcs.h"

#define G_LOG_DOMAIN "nautilus-vcs-git"

struct _NautilusVcsGit
{
    GObject         parent_instance;

    GFile          *location;

    GgitRepository *repository;

    GPtrArray      *branch_list;
    gchar          *active_branch;

    gchar          *head_id;
    gchar          *head_short_id;
    gchar          *head_subject;
    gchar          *head_message;
};

enum
{
    NUM_PROPERTIES = 1,

    PROP_LOCATION
};

static GParamSpec *properties[NUM_PROPERTIES] = { NULL };

static void nautilus_vcs_git_vcs_iface_init (NautilusVcsInterface *iface);
static void nautilus_vcs_git_initable_iface_init (GInitableIface *iface);
static void nautilus_vcs_git_async_initable_iface_init (GAsyncInitableIface *iface);

G_DEFINE_TYPE_EXTENDED (NautilusVcsGit, nautilus_vcs_git,
                        G_TYPE_OBJECT, 0,
                        G_IMPLEMENT_INTERFACE (NAUTILUS_TYPE_VCS,
                                               nautilus_vcs_git_vcs_iface_init)
                        G_IMPLEMENT_INTERFACE (G_TYPE_INITABLE,
                                               nautilus_vcs_git_initable_iface_init)
                        G_IMPLEMENT_INTERFACE (G_TYPE_ASYNC_INITABLE,
                                               nautilus_vcs_git_async_initable_iface_init))

static gchar *
nautilus_vcs_git_get_name (NautilusVcs *vcs)
{
    NautilusVcsGit *self = (NautilusVcsGit *)vcs;

    g_return_val_if_fail (NAUTILUS_IS_VCS_GIT (self), NULL);

    return g_strdup ("git");
}

static GFile *
nautilus_vcs_git_get_location (NautilusVcs *vcs)
{
    NautilusVcsGit *self = (NautilusVcsGit *)vcs;

    g_return_val_if_fail (NAUTILUS_IS_VCS_GIT (self), NULL);

    return self->location;
}

static GPtrArray *
nautilus_vcs_git_get_branch_list (NautilusVcs *vcs)
{
    NautilusVcsGit *self = (NautilusVcsGit *)vcs;

    g_return_val_if_fail (NAUTILUS_IS_VCS_GIT (self), NULL);

    return self->branch_list;
}

static gchar *
nautilus_vcs_git_get_active_branch (NautilusVcs *vcs)
{
    NautilusVcsGit *self = (NautilusVcsGit *)vcs;

    g_return_val_if_fail (NAUTILUS_IS_VCS_GIT (self), NULL);

    return g_strdup (self->active_branch);
}

static gchar *
nautilus_vcs_git_get_head_id (NautilusVcs *vcs)
{
    NautilusVcsGit *self = (NautilusVcsGit *)vcs;

    g_return_val_if_fail (NAUTILUS_IS_VCS_GIT (self), NULL);

    return g_strdup (self->head_short_id);
}

static gchar *
nautilus_vcs_git_get_head_message (NautilusVcs *vcs)
{
    NautilusVcsGit *self = (NautilusVcsGit *)vcs;

    g_return_val_if_fail (NAUTILUS_IS_VCS_GIT (self), NULL);

    return g_strdup (self->head_message);
}

static gchar *
nautilus_vcs_git_get_head_oneline (NautilusVcs *vcs)
{
    NautilusVcsGit *self = (NautilusVcsGit *)vcs;
    gchar *oneline;

    g_return_val_if_fail (NAUTILUS_IS_VCS_GIT (self), NULL);

    if (self->head_short_id != NULL)
        oneline = g_markup_printf_escaped ("<b>%s</b> %s",
                                           self->head_short_id,
                                           self->head_subject);
    else
        oneline = g_strdup (self->head_subject);

    return oneline;
}

static gchar *
nautilus_vcs_git_get_application_name (NautilusVcs *vcs)
{
    NautilusVcsGit *self = (NautilusVcsGit *)vcs;

    g_return_val_if_fail (NAUTILUS_IS_VCS_GIT (self), NULL);

    return g_strdup ("gitg");
}

static void
nautilus_vcs_git_finalize (GObject *object)
{
    NautilusVcsGit *self = NAUTILUS_VCS_GIT (object);

    g_clear_pointer (&self->branch_list, g_ptr_array_free);
    g_clear_pointer (&self->active_branch, g_free);

    g_clear_pointer (&self->head_id, g_free);
    g_clear_pointer (&self->head_short_id, g_free);
    g_clear_pointer (&self->head_message, g_free);

    g_clear_object (&self->repository);

    g_clear_object (&self->location);

    G_OBJECT_CLASS (nautilus_vcs_git_parent_class)->finalize (object);
}

static void
nautilus_vcs_git_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
    NautilusVcsGit *self = NAUTILUS_VCS_GIT (object);

    switch (prop_id)
    {
        case PROP_LOCATION:
            g_value_set_object (value, nautilus_vcs_git_get_location (NAUTILUS_VCS (self)));
            break;

        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
nautilus_vcs_git_set_property (GObject      *object,
                               guint         prop_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
    NautilusVcsGit *self = NAUTILUS_VCS_GIT (object);

    switch (prop_id)
    {
        case PROP_LOCATION:
            g_set_object (&self->location, g_value_get_object (value));
            break;

        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
nautilus_vcs_git_class_init (NautilusVcsGitClass *class)
{
    GObjectClass *object_class;

    object_class = G_OBJECT_CLASS (class);

    object_class->finalize = nautilus_vcs_git_finalize;
    object_class->get_property = nautilus_vcs_git_get_property;
    object_class->set_property = nautilus_vcs_git_set_property;

    g_object_class_override_property (object_class, PROP_LOCATION, "location");
}

static void
nautilus_vcs_git_init (NautilusVcsGit *self)
{
    self->branch_list = g_ptr_array_new_with_free_func (g_free);

    ggit_init ();
}

static void
nautilus_vcs_git_vcs_iface_init (NautilusVcsInterface *iface)
{
    iface->get_name = nautilus_vcs_git_get_name;
    iface->get_location = nautilus_vcs_git_get_location;
    iface->get_branch_list = nautilus_vcs_git_get_branch_list;
    iface->get_active_branch = nautilus_vcs_git_get_active_branch;
    iface->get_head_id = nautilus_vcs_git_get_head_id;
    iface->get_head_message = nautilus_vcs_git_get_head_message;
    iface->get_head_oneline = nautilus_vcs_git_get_head_oneline;
    iface->get_application_name = nautilus_vcs_git_get_application_name;
}

static void
nautilus_vcs_git_load_repository (NautilusVcsGit  *self,
                                  GError         **error)
{
    g_autoptr(GgitBranchEnumerator) enumerator = NULL;
    g_autoptr(GgitRef) reference = NULL;
    g_autoptr(GgitOId) oid = NULL;
    g_autoptr(GgitObject) commit = NULL;
    GError *ggit_error = NULL;

    g_assert (GGIT_IS_REPOSITORY (self->repository));
    g_assert (self->branch_list != NULL);

    g_ptr_array_free (g_ptr_array_ref (self->branch_list), TRUE);

    enumerator = ggit_repository_enumerate_branches (self->repository,
                                                     GGIT_BRANCH_LOCAL, NULL);

    while (ggit_branch_enumerator_next (enumerator))
    {
        g_autoptr(GgitRef) branch = NULL;
        const gchar *name;

        branch = ggit_branch_enumerator_get (enumerator);
        name = g_strdup (ggit_branch_get_name (GGIT_BRANCH (branch), NULL));

        g_ptr_array_add (self->branch_list, (gpointer)name);
    }

    g_free (self->active_branch);

    reference = ggit_repository_get_head (self->repository, NULL);

    if (reference != NULL)
        self->active_branch = g_strdup (ggit_ref_get_shorthand (reference));
    else
        self->active_branch = g_strdup ("master");

    g_free (self->head_id);
    g_free (self->head_short_id);

    if (reference != NULL)
        oid = ggit_ref_get_target (reference);

    if (oid != NULL)
    {
        self->head_id = ggit_oid_to_string (oid);
        self->head_short_id = g_strndup (self->head_id, 6);
    }
    else
        self->head_id = self->head_short_id = NULL;

    g_free (self->head_subject);
    g_free (self->head_message);

    if (oid != NULL)
        commit = ggit_repository_lookup (self->repository, oid,
                                         GGIT_TYPE_COMMIT, NULL);

    if (commit != NULL)
    {
        self->head_subject = g_strdup (ggit_commit_get_subject (GGIT_COMMIT (commit)));
        self->head_message = g_strdup (ggit_commit_get_message (GGIT_COMMIT (commit)));
    }
    else
    {
        self->head_subject = g_strdup ("Your current branch does not have any commits yet");
        self->head_message = g_strdup ("Your current branch does not have any commits yet");
    }
}

static gboolean
nautilus_vcs_git_discover_repository (NautilusVcsGit  *self,
                                      GError         **error)
{
    g_autoptr(GgitRepository) repository = NULL;
    g_autoptr(GFile) location = NULL;
    GError *ggit_error = NULL;

    g_assert (G_IS_FILE (self->location));

    if (g_file_has_uri_scheme (self->location, "trash"))
    {
        g_set_error (error,
                     G_IO_ERROR,
                     G_IO_ERROR_NOT_REGULAR_FILE,
                     "Trash file systems are not supported for git.");
        return FALSE;
    }

    if (!g_file_is_native (self->location))
    {
        g_set_error (error,
                     G_IO_ERROR,
                     G_IO_ERROR_NOT_SUPPORTED,
                     "Only native file systems are supported for git.");
        return FALSE;
    }

    location = ggit_repository_discover (self->location, &ggit_error);

    if (location == NULL)
    {
        g_set_error (error,
                     G_IO_ERROR,
                     G_IO_ERROR_NOT_FOUND,
                     "%s", ggit_error->message);
        g_clear_error (&ggit_error);
        return FALSE;
    }

    g_debug ("%s is part of git repository %s",
             g_file_get_path (self->location),
             g_file_get_path (location));

    g_set_object (&self->location, location);

    repository = ggit_repository_open (self->location, &ggit_error);

    if (repository == NULL)
    {
        g_set_error (error,
                     G_IO_ERROR,
                     G_IO_ERROR_FAILED,
                     "%s", ggit_error->message);
        g_clear_error (&ggit_error);
        return FALSE;
    }

    g_set_object (&self->repository, repository);

    nautilus_vcs_git_load_repository (self, error);

    return TRUE;
}

static gboolean
nautilus_vcs_git_initable_init (GInitable    *initable,
                                GCancellable *cancellable,
                                GError      **error)
{
    NautilusVcsGit *self = (NautilusVcsGit *)initable;

    g_return_val_if_fail (NAUTILUS_IS_VCS_GIT (self), FALSE);

    return nautilus_vcs_git_discover_repository (self, error);
}

static void
nautilus_vcs_git_initable_iface_init (GInitableIface *iface)
{
    iface->init = nautilus_vcs_git_initable_init;
}

static void
nautilus_vcs_git_async_initable_init_worker (GTask *task,
                                             gpointer source_object,
                                             gpointer task_data,
                                             GCancellable *cancellable)
{
    NautilusVcsGit *self = (NautilusVcsGit *)source_object;
    gboolean initialized;
    GError *error = NULL;

    g_assert (G_IS_TASK (task));
    g_assert (NAUTILUS_IS_VCS_GIT (self));
    g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

    initialized = nautilus_vcs_git_discover_repository (self, &error);

    if (initialized == FALSE)
    {
        g_task_return_error (task, error);
        return;
    }

    g_task_return_boolean (task, TRUE);
}

static void
nautilus_vcs_git_async_initable_init_async (GAsyncInitable      *initable,
                                            gint                 io_priority,
                                            GCancellable        *cancellable,
                                            GAsyncReadyCallback  callback,
                                            gpointer             user_data)
{
    NautilusVcsGit *self = (NautilusVcsGit *)initable;
    GTask *task;

    g_return_if_fail (NAUTILUS_IS_VCS_GIT (self));

    task = g_task_new (self, cancellable, callback, user_data);

    g_task_run_in_thread (task, nautilus_vcs_git_async_initable_init_worker);
}

static gboolean
nautilus_vcs_git_async_initable_init_finish (GAsyncInitable  *initable,
                                             GAsyncResult    *result,
                                             GError         **error)
{
    g_return_val_if_fail (g_task_is_valid (result, initable), FALSE);

    return g_task_propagate_boolean (G_TASK (result), error);
}

static void
nautilus_vcs_git_async_initable_iface_init (GAsyncInitableIface *iface)
{
    iface->init_async = nautilus_vcs_git_async_initable_init_async;
    iface->init_finish = nautilus_vcs_git_async_initable_init_finish;
}

/* vim: set tabstop=4 shiftwidth=0 expandtab : */
